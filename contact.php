<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anjani Putra Estates</title>
    <?php include 'stylesheets.php' ?>
</head>

<body>
   <?php include 'header.php'?>

    <!-- sub page main -->
    <div class="subpage-main">
        <!-- header sub page -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <h1 class="h1">Contact</h1>
                        <p>Reach us</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ header sub page  --> 

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-3">
                    <!-- col -->
                    <div class="col-lg-4">
                        <h3 class="h4 fbold py-3">Contact Details</h3>
                        <table class="table">
                            <!-- <tr>
                                <td><span class="icon-phone-hang-up icomoon"></span></td>
                                <td>+91-9395547333, +91-9963796333, +91-8309374151</td>
                            </tr> -->
                            <tr>
                                <td><span class="icon-mail icomoon"></span></td>
                                <td>anjaniputraestates@gmail.com</td>
                            </tr>
                            <tr>
                                <td><span class="icon-pin icomoon"></span></td>
                                <td>10-297,Municipal office road, Mancherial, Telangana - 504208, India</td>
                            </tr>
                        </table>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-8">
                        <h3 class="h4 fbold py-3">Reach us</h3>
                        <?php 
                                if(isset($_POST['submit'])){
                                    
                                $to = "anjaniputraestates@gmail.com"; 
                                $subject = "Mail From ".$_POST['name'];                                
                                $message = "
                                <html>
                                <head>
                                <title>HTML email</title>
                                </head>
                                <body>
                                <p>".$_POST['name']." has sent mail!</p>
                                    <table>
                                        <tr>
                                            <th align='left'>Name</th>
                                            <td>".$_POST['name']."</td>
                                        </tr>
                                        <tr>
                                            <th align='left'>Company Name</th>
                                            <td>".$_POST['companyName']."</td>
                                        </tr>
                                        <tr>
                                            <th align='left'>Email</th>
                                            <td>".$_POST['email']."</td>
                                        </tr>
                                        <tr>
                                            <th align='left'>Phone</th>
                                            <td>".$_POST['phone']."</td>
                                        </tr>
                                        <tr>
                                            <th align='left'>Message</th>
                                            <td>".$_POST['msg']."</td>
                                        </tr>
                                    </table>
                                </body>
                                </html>
                                ";

                                // Always set content-type when sending HTML email
                                $headers = "MIME-Version: 1.0" . "\r\n";
                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                // More headers
                                $headers .= 'From:' .$_POST['name']. "\r\n";
                                mail($to,$subject,$message,$headers);                            
                                echo "<p style='color:green'> Mail Sent. Thank you " . $_POST['name'] .", we will contact you shortly. </p>";
                                }
                            ?>
                            <!-- form -->
                            <form class="form" method="post">
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <div class="input-group">
                                                <input type="text" placeholder="Write Your Name" class="form-control" name="name" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                            <div class="input-group">
                                                <input type="text" placeholder="Write Your Company Name" class="form-control" name="companyName" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Email </label>
                                            <div class="input-group">
                                                <input type="text" placeholder="Write Your Email Address" class="form-control" name="email" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Contact Number </label>
                                            <div class="input-group">
                                                <input type="text" placeholder="Enter Valid Phone Number" class="form-control" name="phone" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Message </label>
                                            <div class="input-group">
                                                <textarea class="form-control" style="height:125px;"
                                                    placeholder="Write Your Custom Message, Not Mandatory" name="msg"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-12">                                           
                                        <input type="submit" value="submit" class="link" name="submit">
                                    </div>
                                    <!--/ col -->

                                </div>
                                <!--/ row -->
                            </form>
                            <!--/ form -->
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /sub page body -->
    </div>
    <!--/ sub page main -->

    <?php include 'footer.php' ?>
</body>

</html>