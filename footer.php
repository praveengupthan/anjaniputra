<!-- footer -->
<footer>
        <a href="javascript:void(0)" id="movetop" class="btnmovetop"><span class="icon-arrow-up icomoon"></span></a>
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row py-md-3">
                <!-- col -->
                <div class="col-lg-4">
                    <p>As property prices of residential apartments skyrocket, investing in a plot is a safer and
                        comparatively affordable way to garner capital appreciation. However, you need to be cautious
                        while striking a land deal. With several land grabbers on the prowl and multiple individuals
                        claiming ownership of a single plot, an investor must comply with all legal formalities when
                        investing in a plot to avoid illegal possession later.</p>
                    <p>
                        <a class="forange" href="about.php">Read More..</a>
                    </p>

                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-4">
                    <h5 class="h4">Navigation</h5>
                    <ul class="footernav">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">About</a></li>
                        <li><a href="services.php">Servies</a></li>
                        <li><a href="projects.php">Projects</a></li>                       
                        <li><a href="contact.php">Contact</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-4">

                    <h5 class="h4">Contact With us</h5>
                    <table>
                        <tr>
                            <td><span class="icon-mobile2 icomoon" style="font-size:20px;"></span></td>
                            <td>+91 9494700707</td>
                        </tr>
                        <tr>
                            <td><span class="icon-mail icomoon" style="font-size:20px;"></span></td>
                            <td>anjaniputraestates@gmail.com</td>
                        </tr>
                        <tr>
                            <td><span class="icon-pin icomoon"></span></td>
                            <td>10-297,Municipal office road, Mancherial, Telangana - 504208, India</td>
                        </tr>
                        
                        
                    </table>
                    <p class="header-social">
                        <a href="https://www.facebook.com/Anjaniputraestates-104807714499017/" target="_blank"><span
                            class="icon-facebook icomoon"></span></a>                               
                        <a href="https://twitter.com/anjaniputraest1" target="_blank"><span
                                class="icon-twitter icomoon"></span></a>
                        <a href="https://www.instagram.com/anjaniputraestates/?hl=en" target="_blank"><span
                        class="icon-instagram icomoon"></span></a>
                    </p>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
            <!-- row -->
            <div class="row copyrights-div">
                <div class="col-lg-12 text-center">
                    <p>All copyrights Reserved @ Anjani Putra Estates Private Limited 2020</p>
                </div>
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </footer>
    <!--/ footer -->

    <!-- script -->
    <script src="js/jquery3.3.1.js"></script>
    <script src="js/bootstrap.min.js"></script>    
    <script src="js/custom.js"></script>
    <script src="js/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
    <!--/ script -->