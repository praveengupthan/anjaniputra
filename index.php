<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anjani Putra Estates</title>
    <?php include 'stylesheets.php' ?>
</head>

<body>
   <?php include 'header.php'?>

    <!-- home slider -->
    <div class="home-slider">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators d-none">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div onclick="window.location.href='anjaniputra-detail.php';" class="carousel-item active" style="background-image: url('./img/slider01.jpg')">
                    <div class="overlay"></div>
                    <div class="carousel-caption">
                        <h3>Open Plots Near Mancherial, Telangana</h3>
                        <p class="d-none d-sm-block">Anjani Putra Estates | Sales | Buy</p>
                    </div>
                </div>
                <!-- Slide Two - Set the background image for this slide in the line below -->
                <div onclick="window.location.href='project-detail.php';" class="carousel-item" style="background-image: url('./img/slider02.jpg')">
                    <div class="overlay"></div>
                    <div class=" carousel-caption">
                        <h3>Kohinoor City Phase-2, Srigandham Trees Plantation</h3>
                        <p class="d-none d-sm-block">Srigandham trees were the property of the government for years, but
                            now the rule has changed. As per the Section 108 of the Telangana Forest (Amendment) Act
                            2001, Srigandham tree grown in a specific land is the property of the owner of the land.</p>
                    </div>
                </div>
                <!-- Slide Three - Set the background image for this slide in the line below -->
                <div onclick="window.location.href='anjaniputra-detail.php';" class="carousel-item" style="background-image: url('./img/slider03.jpg')">
                    <div class="carousel-caption">
                        <h3>Welcome to Anjaniputra Estates</h3>
                        <p class="d-none d-sm-block">Anjani Putra Estates is blessed with a professionally qualified
                            team of experts with decades of expertise in the realm of real estate Commercial Plots, Open
                            Plots and Residential Plots. With a passion for innovation and a dedication to customers.
                        </p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!--/ home slider -->

    <!-- about -->
    <div class="about">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4 top-about">
                    <img src="img/about01.jpg" alt="" class="img-fluid">
                    <article class="pt-md-2">
                        <h3 class="h6 fbold text-uppercase">Our Vision</h3>
                        <p>The vision of Anjani Putra Estates is to work continuously towards developing an affordable,
                            yet world-class benchmark for open plots, ventures, residential and commercial properties.
                        </p>
                    </article>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4 top-about">
                    <img src="img/about02.jpg" alt="" class="img-fluid">
                    <article class="pt-md-2">
                        <h3 class="h6 fbold text-uppercase">Our Mission</h3>
                        <p>At Anjani Putra Estates, it is our mission to design, build & market HMDA and DTCP approved
                            plots, residential and commercial properties of world class quality while ensuring the
                            highest possible level of satisfaction to our valued customers.</p>
                    </article>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4 top-about">
                    <img src="img/about03.jpg" alt="" class="img-fluid">
                    <article class="pt-md-2">
                        <h3 class="h6 fbold text-uppercase">Quality</h3>
                        <p>Driven by quality, we blend professionalism and expertise to incorporate your ideas with our
                            plans to deliver you such a project that offers total satisfaction.</p>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->

        <!-- container -->
        <div class="container bottom-about">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 align-self-center">
                    <h1 class="h5 fbold fwhite text-uppercase">LEADING THE WAY LOCATE THE BETTER LIFE</h1>
                    <p>Anajaniputra Estates has established itself as one of Hyderabad largest property developers
                        specializing in building quality apartments as well as state of the art
                        commercial spaces. For the astute investor, our properties offer better appreciation due to
                        location advantages, value for money pricing and better build quality. </p>
                    <p>
                        <a href="about.php" class="btn-orange">Read More</a>
                    </p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6">
                    <img src="img/about4.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!-- / about -->

    <!-- services -->
    <div class="services">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4">
                    <div class="servicecol">
                        <h3 class="d-flex">
                            <span class="icon-room icomoon"></span>
                            <span class="txt">Open Plot Marketing</span>
                        </h3>
                        <p>Investments in land property, residential plot or farmlands are considered profitable options
                            for a long-term perspective in India as well as across the world. </p>
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4">
                    <div class="servicecol">
                        <h3 class="d-flex">
                            <span class="icon-worker icomoon"></span>
                            <span class="txt">Layout Development</span>
                        </h3>
                        <p>Throughout our completed and ongoing ventures in Hyderabad, we have maintained and delivered
                            unparalleled quality. Right from hand-picking the approved sites with complete document work
                            to designing the layout to developing the amenities, we are a complete one-stop property
                            destination.</p>
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4">
                    <div class="servicecol">
                        <h3 class="d-flex">
                            <span class="icon-Page-1 icomoon"></span>
                            <span class="txt">24x7 Security for Layouts</span>
                        </h3>
                        <p>Even though first we meet our clients at our office, we have to provide our services at the
                            farmland only. By visiting the farmland we can assess various factors which will decide the
                            crop to be cultivated at the farmland.</p>
                    </div>
                </div>
                <!--/ col -->

            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ services -->

    <!-- current project -->
    <div class="current-project">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 pl-md-0">
                    <!-- image gallery -->
                    <div id="currentproject" class="carousel slide" data-ride="carousel">
                        <span class="orangespan">Current Project</span>
                        <ol class="carousel-indicators">
                            <li data-target="#currentproject" data-slide-to="0" class="active">
                                <img src="img/kohinoor-gallery/gallery12.jpg" alt="">
                            </li>
                            <li data-target="#currentproject" data-slide-to="1">
                                <img src="img/kohinoor-gallery/gallery13.jpg" alt="">
                            </li>
                            <li data-target="#currentproject" data-slide-to="2">
                                <img src="img/kohinoor-gallery/gallery14.jpg" alt="">
                            </li>
                            <li data-target="#currentproject" data-slide-to="3">
                                <img src="img/kohinoor-gallery/gallery15.jpg" alt="">
                            </li>

                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="img/kohinoor-gallery/gallery12.jpg" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/kohinoor-gallery/gallery13.jpg" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/kohinoor-gallery/gallery14.jpg" alt="Third slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/kohinoor-gallery/gallery14.jpg" alt="Third slide">
                            </div>
                        </div>
                    </div>
                    <!--/ image gallery -->
                </div>
                <!--col -->
                <!-- col -->
                <div class="col-lg-6 align-self-center">
                    <article class="">
                        <h3 class="h4 text-uppercase fblue fbold">Anjani Putra Estates, Kohinoor Phase II, Jaipur.
                        </h3>
                        <p class="text-justify pb-3">Kohinoor Phase II Project aims to bring happiness and prosperity to all. Located in the pristine surrounding of the Eastern Ghats, the Kohinoor Phase presents a stunning visual treat for everyone. Our team that includes of Srigandham Plant-business experts adheres to the highest values and standards to sure project success and high rate of returns for the investors. </p>

                        <p>Kohinoor Phase Project brings two significant benefits to all its investors. Ownership of the farm unit which you could sell or retain at the end of 15 year lease period; and enjoy returns from the sandalwood plantation without working on it.</p>

                        <ul class="list-items mb-4">
                            <li>Near Jaipur City</li>
                            <li>100% Vaastu</li>
                            <li>2nd Bit on National Highway 63</li>
                            <li>10 Min to Manchiryal District</li>
                            <li>10 Min Journey to Godavari Khani</li>
                            <li>Clear Titles, Spot Registration</li>
                            <li>Residential Jone</li>
                            <li>Play Ground for Children</li>
                           
                        </ul>
                        <a href="project-detail.php" class="btn-orange mt-5">Read More</a>

                    </article>
                </div>
                <!--col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ contianer fluid -->
    </div>
    <!--/ current project -->

    <!-- all projects -->
    <div class="projects-list">
        <!-- container -->
        <div class="container">
            <h4 class="text-center h4 fbold text-uppercase fblue pt-md-5 pb-md-3">Our Projects</h4>
        </div>
        <!--/ container -->
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-3">
                    <figure class="project-figure">
                        <a href="project-detail.php"><img src="img/project01.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h5><a href="project-detail.php">Anjani Putra Estates 01</a></h5> 
                        </article>
                    </figure>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3">
                    <figure class="project-figure">
                        <a href="anjaniputra-detail.php"><img src="img/project02.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h5><a href="anjaniputra-detail.php">Anjani Putra Estates 02</a></h5>                           
                        </article>
                    </figure>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3">
                    <figure class="project-figure">
                    <a href="project-detail.php"><img src="img/project03.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h5><a href="project-detail.php">Anjani Putra Estates 03</a></h5>                            
                        </article>
                    </figure>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3">
                    <figure class="project-figure">
                        <a href="project-detail.php"><img src="img/project04.jpg" alt="" class="img-fluid"></a>
                        <article>
                            <h5><a href="project-detail.php">Anjani Putra Estates 04</a></h5>                            
                        </article>
                    </figure>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->
    </div>
    <!-- /all projects -->

    <?php include 'footer.php' ?>
</body>

</html>