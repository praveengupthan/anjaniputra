<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anjani Putra Estates</title>
    <?php include 'stylesheets.php' ?>
</head>

<body>
   <?php include 'header.php'?>

    <!-- sub page main -->
    <div class="subpage-main">
        <!-- header sub page -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <h1 class="h1">Projects</h1>
                        <p>Investing in Sandalwood Cultivation is a better choice for sustainable living, protecting the environment and encouraging going green concept.</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ header sub page  --> 

        <!-- sub page body -->
        <div class="subpage-body">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row pt-4">
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="img/slider01.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <h2 class="h4 fbold">Anjaniputra Estates</h2>
                        <p class="pb-3">Locatio: <span class="subtitle">Manchirial, Telangana</span></p>
                        <p class="text-justify pb-4">Anjaniputra Estates – The organization is enlisted in 2018 and we build up the best-gated network offering predominant plots available to be purchased in Manchiryal, Telangana. On the off chance that you are searching for a chance to put resources into Open plots or for structure an extravagance estate in an enormous rustic retreat, ... </p>
                        <p>
                            <a href="project-detail.php" class="btn-orange">Read More</a>
                        </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row pt-4">
                    <!-- col -->
                    <div class="col-lg-6 order-lg-last">
                        <img src="img/slider02.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <h2 class="h4 fbold">Kohinoor City Phase 2</h2>
                        <p class="pb-3">Locatio: <span class="subtitle">Hyderabad, Telangana</span></p>
                        <p class="text-justify pb-4">Sandalwood is a valuable commodity, traded for centuries and well known in many markets. Many sandalwood products are sold in luxury markets. That's why there is a growth in Sri Gandham Plants. It is even used in preparing medicinal products. ... </p>
                        <p>
                            <a href="project-detail.php" class="btn-orange">Read More</a>
                        </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row pt-4">
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="img/slider03.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <h2 class="h4 fbold">Anjaniputra Estates Phase 2</h2>
                        <p class="pb-3">Locatio: <span class="subtitle">Hyderabad, LB Nagar, Telangana</span></p>
                        <p class="text-justify pb-4">Anjaniputra has a dedicated team of professionals who are focused on delivering quantifiable results and returns for clients and advancing Sandalwood Farm Land in Hyderabad with a global perspective. Anjaniputra provides the ability to source and acquires land suitable for commercial sandalwood plantations. ... </p>
                        <p>
                            <a href="project-detail.php" class="btn-orange">Read More</a>
                        </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                  <!-- row -->
                  <div class="row pt-4">
                    <!-- col -->
                    <div class="col-lg-6 order-lg-last">
                        <img src="img/slider04.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <h2 class="h4 fbold">Anjaniputra Estates Phase 3</h2>
                        <p class="pb-3">Locatio: <span class="subtitle">Hyderabad, Kokapet, Telangana</span></p>
                        <p class="text-justify pb-4">Anjaniputra has a dedicated team of professionals who are focused on delivering quantifiable results and returns for clients and advancing Sandalwood Farm Land in Hyderabad with a global perspective. Anjaniputra provides the ability to source and acquires land suitable for commercial sandalwood plantations. ... </p>
                        <p>
                            <a href="project-detail.php" class="btn-orange">Read More</a>
                        </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->


            </div>
            <!-- /container -->

        </div>
        <!-- /sub page body -->
    </div>
    <!--/ sub page main -->

    <?php include 'footer.php' ?>
</body>

</html>