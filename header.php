 <!-- header -->
 <header class="fixed-top">
        <!-- top header -->
        <div class="top-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-end">
                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="d-flex justify-content-between">
                            <p>Ph: +91 9494700707</p>
                            <p>anjaniputraestates@gmail.com</p>
                            <p class="header-social">
                                <a href="https://www.facebook.com/Anjaniputraestates-104807714499017/" target="_blank"><span
                                        class="icon-facebook icomoon"></span></a>                               
                                <a href="https://twitter.com/anjaniputraest1" target="_blank"><span
                                        class="icon-twitter icomoon"></span></a>
                                <a href="https://www.instagram.com/anjaniputraestates/?hl=en" target="_blank"><span
                                class="icon-instagram icomoon"></span></a>
                            </p>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ top header -->
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container">
                <a class="navbar-brand" href="index.php"><img src="img/logo.svg" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                    aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-menu icomoon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">
                                <span>Home</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about.php">
                                <span>About</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="services.php">
                                <span>Services</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Projects
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="project-detail.php">Kohinoor City</a>
                            <a class="dropdown-item" href="anjaniputra-detail.php">Anjaniputra Estates</a>
                            <a class="dropdown-item" href="javascript:void(0)">Anjaniputra Estates II</a>
                            <a class="dropdown-item" href="javascript:void(0)">Anjaniputra Estates III</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="projects.php">All Projects</a>
                            </div>
                        </li>                      
                        <li class="nav-item">
                            <a class="nav-link" href="contact.php">
                                <span>Contact</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!--/ header -->