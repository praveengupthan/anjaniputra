<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anjani Putra Estates</title>
    <?php include 'stylesheets.php' ?>
</head>

<body>
   <?php include 'header.php'?>

    <!-- sub page main -->
    <div class="subpage-main">
        <!-- header sub page -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <h1 class="h1">Services</h1>
                        <p>Those looking for Open Land Buyers & Consultants In Hyderabad &amp; Telangana can trust in Anjani Putra Estates</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ header sub page  --> 

        <!-- sub page body -->
        <div class="subpage-body">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row pt-4 justify-content-center">
                    <!-- col -->
                    <div class="col-lg-10 text-center">
                        <img src="img/slider01.jpg" alt="" class="img-fluid">
                        <article class="text-center p-3">
                            <h2 class="h4 fbold">Open Lands Consultant</h2>                       
                            <p class="text-center pb-4">We take immense pleasure in offering 100% assistance in Buying Property in Hyderabad. The company has a large databank of Properties on Sale offers in Hyderabad.  </p>  
                        </article>
                    </div>
                    <!--/ col -->                   
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row pt-4 justify-content-center">
                    <!-- col -->
                    <div class="col-lg-10 textr-center">
                        <img src="img/slider02.jpg" alt="" class="img-fluid">
                        <article class="text-center p-3">
                            <h2 class="h4 fbold">Sreegandham Tree Development Consultant</h2>                       
                            <p class="text-center pb-4">The company has sold enormous sandalwood saplings to farmers in the states of Karnataka, Andhra Pradesh, Tamil Nadu, U.P and Maharashtra. As a matter of fact, we have gained new customers from our existing clients. We believe in catering. </p> 
                        </article>
                    </div>
                    <!--/ col -->                   
                </div>
                <!--/ row -->
            </div>
            <!-- /container -->

        </div>
        <!-- /sub page body -->
    </div>
    <!--/ sub page main -->

    <?php include 'footer.php' ?>
</body>

</html>