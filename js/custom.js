$(window).scroll(function () {
    if ($(this).scrollTop() > 10) {
        $(".fixed-top").addClass("fixed-theme");
    } else {
        $(".fixed-top").removeClass("fixed-theme");
    }
});


//click on move browser top
$(document).ready(function () {
    //window scroll top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#movetop').fadeIn();
        } else {
            $('#movetop').fadeOut();
        }
    });
    //Click event to scroll to top
    $('#movetop').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});

