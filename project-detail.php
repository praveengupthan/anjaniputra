<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anjani Putra Estates</title>
    <?php include 'stylesheets.php' ?>
</head>

<body>
   <?php include 'header.php'?>

    <!-- sub page main -->
    <div class="subpage-main">
        <!-- header sub page -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <h1 class="h1">Kohinoor City Phase 1</h1>
                        <p>Farmland, Jaipur</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ header sub page  --> 

        <!-- sub page body -->
        <div class="subpage-body">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row pt-4">
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="img/slider01.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <h2 class="h4 fbold">Project Overview</h2>                       
                        <p class="text-justify pb-4">Anjaniputra specialises in Red Sandalwood Plantation. Over the years, Anjaniputra has accumulated a wealth of knowledge and experience in developing Sandalwood Plantation in Hyderabad and Telangana. Anjaniputra offers its clients a complete lifecycle solution for investors seeking to directly own Sandalwood Cultivation.</p>
                        <p>Sandalwood is a valuable commodity, traded for centuries and well known in many markets. Many sandalwood products are sold in luxury markets. That's why there is a growth in Sri Gandham Plants. It is even used in preparing medicinal products.</p>                       
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!-- /container -->

            <!-- full div -->
            <div class="fulldiv">
                <!--container -->
                <div class="container">
                    <h3 class="h4 fbold py-3">Specifications</h3>
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6">
                            <ul class="list-items">
                                <li>Electrical connections with armoured cables are as per IS make of HAVELLS brand, 1.1KV grade cables conductor composed of Solid Aluminum, XLPE insulated cores laid up inners-heated and overall PVC outer-heated cable confirmed as per IS: 7098 part-1.</li>

                                <li>Borewell water supply through high-density polyethene (HDPE) pipes make of the supreme brand inside each plot for direct water connection.</li>

                                <li>Roads: 100 feet main road laid and 30 feet internal blacktopped roads over mix bed concrete kerb-stones and saucer drains respectively</li>

                                <li>24 hours security at the entrance gate and security office with access control services.</li>

                                <li>High Street lighting</li>

                                <li>Harvesting Chambers for rainwater percolation</li>
                            </ul>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                         <ul class="list-items">
                                <li>All cables are installed through underground and provision for Individual Intercom Telephone Systems, CCTV Surveillance system, DTH services, WIFI accessibility is provided.</li>

                                <li>Lush green parks with thick well-grown plantation. Ample parks in the layout providing to-do activities like walking, secure play area for children, exercises and many more.</li>

                                <li>Underground Sump and 15meters high overhead tank for assured water supply with an overall storage capacity of 2 lakh litres.</li>

                                <li>Drainage connection with Eco –Drain Pipes made of supreme brand</li>

                                <li>Gated plotted development with boundary walls</li>
                               </ul>
                         </div>
                        <!--/ col -->
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ full div -->

            <!-- container -->
            <div class="container">
                <h3 class="h4 fbold py-3">Image Gallery</h3>
                <div class="gallery-section grid-gallery">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery27.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery27.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery28.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery28.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery12.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery12.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery13.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery13.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery14.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery14.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery15.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery15.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery16.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery16.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery17.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery17.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery18.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery18.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery19.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery19.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery20.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery20.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery21.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery21.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery22.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery22.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery23.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery23.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery24.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery24.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery25.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery25.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4 col-sm-6 item">
                            <a class="lightbox" href="./img/kohinoor-gallery/gallery26.jpg">
                                <img class="img-fluid image scale-on-hover" src="./img/kohinoor-gallery/gallery26.jpg">
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!--/ container -->

            <!-- full div -->
            <div class="fulldiv">
                <!--container -->
                <div class="container">
                    <h3 class="h4 fbold py-3">Highlights: The future is coming here</h3>
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6">
                            <ul class="list-items">
                                <li>Gachibowli Continental hospitals financial district and ORR25 min drive away</li>

                                <li>Rajiv Gandhi International Airport 15 minute drive away</li>

                                <li>ICFAI Business School and IIT Kandi 20 minutes drive away</li>

                                <li>Lahari Resorts less than 2 kilometres away</li>

                                <li>Indus International School 15 minute drive away and other schools like Oakridge International School, Delhi Public School, Chirec Public Schools 40 minute drive</li>
                               
                            </ul>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                         <ul class="list-items">
                                <li>BDL Township and nearby factories in 2 kilometres away</li>

                                <li>Other colleges within a 30-minute drive CBIT, MGIT and the University of Hyderabad.</li>

                                <li>Treasure Island, Golconda Resorts and Spa 30 minute drive away</li>

                                <li>Songs of the earth are less than 1 kilometre away</li>
                                
                            </ul>
                         </div>
                        <!--/ col -->
                    </div>
                </div>
                <!--/ container -->
            </div>
            <!--/ full div -->

            <!-- map -->
            <div class="map-location mb-3">
             <!--container -->
             <div class="container">
                    <h3 class="h4 fbold py-3">Reach us</h3>
                </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15225.895442867586!2d78.44108889999998!3d17.43701955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1584787693205!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
            <!-- map -->

        </div>
        <!-- /sub page body -->
    </div>
    <!--/ sub page main -->

    <?php include 'footer.php' ?>
</body>

</html>