<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anjani Putra Estates</title>
    <?php include 'stylesheets.php' ?>
</head>

<body>
   <?php include 'header.php'?>

    <!-- sub page main -->
    <div class="subpage-main">
        <!-- header sub page -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 text-center">
                        <h1 class="h1">About Anjaniputra Estates</h1>                        
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ header sub page  --> 

        <!-- sub page body -->
        <div class="subpage-body">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row pt-4">
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="img/about4.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <h2 class="h4 fbold">Overview</h2>                       
                        <p class="text-justify pb-4">Anjaniputra specialises in Red Sandalwood Plantation. Over the years, Anjaniputra has accumulated a wealth of knowledge and experience in developing Sandalwood Plantation in Hyderabad and Telangana. Anjaniputra offers its clients a complete lifecycle solution for investors seeking to directly own Sandalwood Cultivation.</p>
                        <p>Sandalwood is a valuable commodity, traded for centuries and well known in many markets. Many sandalwood products are sold in luxury markets. That's why there is a growth in Sri Gandham Plants. It is even used in preparing medicinal products.</p>                       
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row py-3">
                    
                     <!-- col -->
                     <div class="col-lg-12">
                        <h5 class="h5 fbold">The company offers a diverse and innovative portfolio of property types and styles, each an example of superior living, by regaining supremacy Red Sandalwood cultivation all taken over to suit the highest standards, significantly considering the environment and long term sustainability when developing a site.</h5>
                        <p>Integrity, commitment to excellence, market knowledge and wealth of experience are the company's key attributes to success.</p>
                        <p>Our promise is to provide value to clients in terms of quality, design, service, support and investment returns.</p>
                        <p>Investing in Sandalwood Cultivation is a better choice for sustainable living, protecting the environment and encouraging going green concept.</p>
                        <p>Our commitment is to provide absolute constant customer satisfaction by considering the emotional and practical priorities of the residents right from designing, planning to final finishing touches.</p>
                     </div>
                    <!--/ col -->                   
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row py-4">
                    <!-- col -->
                    <div class="col-lg-4">
                        <div class="col-section">
                            <span class="icon-calculator icomoon h1"></span>
                            <h4 class="h4 fbold py-1 forange">Invest</h4>
                            <p>Anjaniputra Projects has ethically designed partner offers to suit your investment budget. An investment for you and your loved one's secure future</p>
                        </div>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-4">
                        <div class="col-section">
                            <span class="icon-leaf icomoon h1"></span>
                            <h4 class="h4 fbold py-1 forange">We Cultivate</h4>
                            <p>We offer scientific crop management and consequent healthy growth of the Red Sandalwood plants together with robust security to the farm land.</p>
                        </div>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-4">
                        <div class="col-section">
                            <span class="icon-pie-chart icomoon h1"></span>
                            <h4 class="h4 fbold py-1 forange">Share Profits</h4>
                            <p>At the end of the harvest period, proceeds to be handed over to the individual owners The returns will be divided between the owners and Lahari Farm Lands on 50:50 percent basis.</p>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!-- /container -->

          



          

        </div>
        <!-- /sub page body -->
    </div>
    <!--/ sub page main -->

    <?php include 'footer.php' ?>
</body>

</html>